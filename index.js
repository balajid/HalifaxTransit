var express = require('express')

var app = express()

const path = require('path')

var power = require(path.join(__dirname,'public/js/power.json'))

var powerCorr = require(path.join(__dirname,'public/js/powerCorr.json'))


var GtfsRealtimeBindings = require('gtfs-realtime-bindings');
var request = require('request');




//console.log(power)

app.use(express.static(path.join(__dirname,'public')))

app.set('view engine','ejs')
var requestSettings = {
  method: 'GET',
  url: 'http://gtfs.halifax.ca/realtime/Vehicle/VehiclePositions.pb',
  encoding: null
}

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const csvWriter = createCsvWriter({
  path: path.join(__dirname,'public/transit10.csv'),
  header: [
      {id: 'trip_id', title: 'BUSID'},
      {id: 'lat', title: 'LATITUDE'},
      {id: 'lon', title: 'LONGITUDE'},
      {id: 'speed', title: 'SPEED'},
      {id:'bearing', title:'BEARING'},
      {id:'time', title:'TIME'},
      {id:'date', title:'DATE'}
    ]
  });

app.get('/',(req,res) => {





  var vehicles=[]
  console.log("in")
  request(requestSettings, function (error, response, body) {
    console.log("in")
    console.log(error,response.statusCode)
    if (!error && response.statusCode == 200) {
      console.log("in")
      var feed = GtfsRealtimeBindings.FeedMessage.decode(body);
      //console.log(feed)
      feed.entity.forEach(function(entity) {
        if (entity.trip_update) {
          //console.log(entity.trip_update);
        }
        vehicles.push(entity.vehicle)

      });
      var pos=[];
      console.log(vehicles)
      for(var i=0;i<vehicles.length;i++){
          var temp={}
          temp['lat']=vehicles[i]['position'].latitude;
          temp['lon']=vehicles[i]['position'].longitude;
          pos.push(temp)


      }
      //console.log(pos);
    }

    res.render('leaftut3',{poww:JSON.stringify(power),powwCorr:JSON.stringify(powerCorr),pos:JSON.stringify(pos)})



  });



});

app.get('/vehicle',(req,res) => {
  var vehicles1=[]
  request(requestSettings, function (error, response, body) {
    //console.log("in")
    //console.log(error,response.statusCode);
    if (!error && response.statusCode == 200) {
      //console.log("in")
      var feed = GtfsRealtimeBindings.FeedMessage.decode(body);
      //console.log(feed)
      feed.entity.forEach(function(entity) {
        if (entity.trip_update) {
          //console.log(entity.trip_update);
        }
        vehicles1.push(entity.vehicle)

      });
      var pos1=[];
      var records=[];
      for(var i=0;i<vehicles1.length;i++){
          var record={}
          var temp={}
          temp['lat']=vehicles1[i]['position'].latitude;
          temp['lon']=vehicles1[i]['position'].longitude;
          pos1.push(temp)
          record['trip_id']=vehicles1[i]['trip'].route_id;
          record['lat']=vehicles1[i]['position'].latitude;
          record['lon']=vehicles1[i]['position'].longitude;
          record['speed']=((!vehicles1[i]['position'].speed)?0:(vehicles1[i]['position'].speed));
          record['bearing']=((!vehicles1[i]['position'].bearing)?0:(vehicles1[i]['position'].bearing));
          const temp2=vehicles1[i]['timestamp'].low;
          var date=new Date(temp2*1000);
          var time= date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
          record['time']=time;
          record['date']=vehicles1[i]['trip'].start_date;
          records.push(record);
      }
      //console.log(records);
      csvWriter.writeRecords(records)       // returns a promise
        .then(() => {
              console.log('...Done');
        });
    }
    res.send(JSON.stringify(pos1))
  });
});

app.get('/getter',(req,res) => {
  console.log(req.query.val)
    const value = req.query.val;
    if(value=="Power Correction"){
        res.send(JSON.stringify(powerCorr))
    }
    else{
       res.send(JSON.stringify(power))
    }

});

app.listen(2600,"0.0.0.0", () => {

    console.log('the server running on port 2600')

});
